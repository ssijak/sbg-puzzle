import hashlib


class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None


class Tree:
    _preorder_index = 0

    def __init__(self, ino, preo):

        def create_tree(inoreder, preorder, inoder_start, inoreder_end):
            """ In preorder list, leftmost value is the root of the tree. If we find that member in inordered list then
            we know the root and all the left member (left from the root member in inordered list) and right member
            children of the root node. Than recursively we repeat the process and reconstruct the tree.
            """
            if inoder_start > inoreder_end:
                return None

            node = Node(preorder[self._preorder_index])
            self._preorder_index += 1

            if inoder_start == inoreder_end:
                return node

            inorder_index = [i for i in range(inoder_start, inoreder_end + 1) if inoreder[i] == node.value][0]

            node.left = create_tree(inoreder, preorder, inoder_start, inorder_index - 1)
            node.right = create_tree(inoreder, preorder, inorder_index + 1, inoreder_end)

            return node

        self.root_node = create_tree(ino, preo, 0, len(ino) - 1)

    def get_leaf_nodes_left_oredered(self):
        result = []

        def traverse(node):
            if node is None:
                return

            # getting nodes without children
            if node.left is None and node.right is None:
                result.append(node)

            traverse(node.left)
            traverse(node.right)

        traverse(self.root_node)

        return result


def get_file_words(file_name):
    with open(file_name, "r") as f:
        return f.read().split()


def get_md5_from_list(ls):
    return hashlib.md5("".join(ls).encode('utf-8')).hexdigest()


if __name__ == "__main__":
    inorder = get_file_words("inorder.txt")
    preorder = get_file_words("preorder.txt")

    tree = Tree(inorder, preorder)
    leafs = tree.get_leaf_nodes_left_oredered()
    leafs_data = [leaf.value for leaf in leafs]
    print(get_md5_from_list(leafs_data))
